package ro.codetwisters.hackathonsv.common.provider;

import android.text.TextUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 */
public class ProviderManager {

    private static final ProviderManager instance = new ProviderManager();

    public static ProviderManager getInstance() {
        return instance;
    }

    private Map<ProviderKey, ProviderHolder> registeredProviders = new HashMap<>();
    private final Object lock = new Object();

    public void registerProvider(Object provider) {
        synchronized (lock) {
            Method[] methods = provider.getClass().getDeclaredMethods();
            for (Method method : methods) {
                Provides provides = method.getAnnotation(Provides.class);
                // method needs to have a single parameter annotated with HandlesEvent.class
                if (provides != null) {
                    // find HandlesEvent annotation
                    method.setAccessible(true);
                    registeredProviders.put(new ProviderKey(method.getReturnType(), provides.value()), new ProviderHolder(provider, method));
                }
            }
        }
    }

    public void unregisterProvider(Object provider) {
        synchronized (lock) {
            Method[] methods = provider.getClass().getDeclaredMethods();
            for (Method method : methods) {
                Provides provides = method.getAnnotation(Provides.class);
                // method needs to have a single parameter annotated with HandlesEvent.class
                if (provides != null) {
                    // find HandlesEvent annotation
                    method.setAccessible(true);
                    registeredProviders.remove(new ProviderKey(method.getReturnType(), provides.value()));
                }
            }
        }
    }

    public <T> T getProvidedValue(Class<T> tClass) {
        return getProvidedValue(tClass, null);
    }

    public <T> T getProvidedValue(Class<T> tClass, String identifier) {
        ProviderHolder providerHolder = registeredProviders.get(new ProviderKey(tClass, identifier));
        if (providerHolder != null) {
            try {
                // this is checked by the registration mechanism,
                // we know for sure this method has the appropriate return type
                //noinspection unchecked
                return (T) providerHolder.providerMethod.invoke(providerHolder.providerObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static class ProviderKey {
        private Class providedClass;
        private String providerId;

        private ProviderKey(Class providedClass, String providerId) {
            this.providedClass = providedClass;
            this.providerId = providerId;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof ProviderKey && ((ProviderKey) o).providedClass.equals(providedClass) && TextUtils.equals(((ProviderKey) o).providerId, providerId);
        }
    }

    private static class ProviderHolder {
        private Object providerObject;
        private Method providerMethod;

        private ProviderHolder(Object providerObject, Method providerMethod) {
            this.providerObject = providerObject;
            this.providerMethod = providerMethod;
        }
    }

}
