package ro.codetwisters.hackathonsv.common.messaging;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Event dispatching mechanism<br />
 * Usage:<br />
 * <b>EventDispatcher.INSTANCE.registerEventHandler(someInstance)</b> registers <i>someInstance</i> to be an event handler on the methods that have annotated parameters with <i>HandlesEvent</i><br />
 * <b>EventDispatcher.INSTANCE.fire(eventObj)</b> fires an event that is carried out by the <i>eventObj</i> calls the methods registered with the event handler by reflection<br />
 * <b>EventDispatcher.INSTANCE.unregisterEventHandler(someInstance)</b> use this to remove this object from the event handlers, usually to avoid memory leaks
 */
public enum EventDispatcher {
    INSTANCE;

    private Map<Class, List<EventListener>> eventListeners = new HashMap<>();
    private final Object lock = new Object();

    /**
     * adds this event handler to the list of event handlers based on methods with parameters that are annotated with <b>HandlesEvent</b>
     * @param eventHandler the instance that will be parsed for handler methods
     */
    public void registerEventHandler(Object eventHandler) {
        synchronized (lock) {
            Method[] methods = eventHandler.getClass().getDeclaredMethods();
            for (Method method : methods) {
                Annotation[][] annotations = method.getParameterAnnotations();
                // method needs to have a single parameter annotated with HandlesEvent.class
                if (annotations.length == 1) {
                    // find HandlesEvent annotation
                    for (Annotation annotation : annotations[0]) {
                        if (annotation.annotationType().equals(HandlesEvent.class)) {
                            method.setAccessible(true);
                            Class[] types = method.getParameterTypes();
                            addEventListener(types[0], new EventListener(eventHandler, method));
                        }
                    }
                }
            }
        }
    }

    /**
     * remove event handlers registered with EventDispatcher.registerEventHandler()
     * @param eventHandler the instance that has the handler methods registered
     */
    public void unregisterEventHandler(Object eventHandler) {
        synchronized (lock) {
            Method[] methods = eventHandler.getClass().getDeclaredMethods();
            for (Method method : methods) {
                Annotation[][] annotations = method.getParameterAnnotations();
                // method needs to have a single parameter annotated with HandlesEvent.class
                if (annotations.length == 1) {
                    // find HandlesEvent annotation
                    for (Annotation annotation : annotations[0]) {
                        if (annotation.annotationType().equals(HandlesEvent.class)) {
                            Class[] types = method.getParameterTypes();
                            removeEventListener(types[0], eventHandler);
                        }
                    }
                }
            }
        }
    }

    /**
     * send an event to all the method handlers registered with this EventDispatcher that have the same class as the Object <b>event</b>
     * @param event this instance will be used to pass as a parameter when calling the event handler methods via reflection
     */
    public void fire(Object event) {
        synchronized (lock) {
            List<EventListener> listeners = eventListeners.get(event.getClass());
            if (listeners != null) {
                for (EventListener listener : listeners) {
                    try {
                        listener.method.invoke(listener.listener, event);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void addEventListener(Class aClass, EventListener listener) {
        List<EventListener> listeners = eventListeners.get(aClass);
        if (listeners == null) {
            listeners = new ArrayList<>();
            eventListeners.put(aClass, listeners);
        }
        listeners.add(listener);
    }

    private void removeEventListener(Class aClass, Object listener) {
        List<EventListener> listeners = eventListeners.get(aClass);
        if (listeners != null) {
            Iterator<EventListener> listenerIterator = listeners.iterator();
            while (listenerIterator.hasNext()) {
                EventListener eventListener = listenerIterator.next();
                if (eventListener.listener.equals(listener)) {
                    listenerIterator.remove();
                }
            }
            if (listeners.size() == 0) {
                eventListeners.remove(aClass);
            }
        }
    }

    private static class EventListener {
        Object listener;
        Method method;

        private EventListener(Object listener, Method method) {
            this.listener = listener;
            this.method = method;
        }
    }

}
