package ro.codetwisters.hackathonsv.common.injection;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CtInjector {

    public static void setupActivity(Activity activity) {
        activity.setContentView(getLayoutResource(activity));
        attachAnnotatedViews(activity);
    }

    public static View setupFragment(Object fragment, LayoutInflater inflater, ViewGroup container) {
        InjectLayout layoutAnnotation = fragment.getClass().getAnnotation(InjectLayout.class);
        View rootView = inflater.inflate(layoutAnnotation.value(), container, false);
        CtInjector.attachAnnotatedViews(fragment, rootView);
        return rootView;
    }

    public static void attachAnnotatedViews(final Object object) {
        attachAnnotatedViews(object, object);
    }

    public static void attachAnnotatedViews(final Object object, final Object rootViewProvider) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            InjectView viewAnnotation = field.getAnnotation(InjectView.class);
            if (viewAnnotation != null) {
                field.setAccessible(true);
                try {
                    field.set(object, getViewFromProvider(rootViewProvider, viewAnnotation.value()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        Method[] methods = object.getClass().getDeclaredMethods();
        for (final Method method : methods) {
            AttachClick clickAnnotation = method.getAnnotation(AttachClick.class);
            if (clickAnnotation != null) {
                method.setAccessible(true);
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            method.invoke(object);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                };
                getViewFromProvider(rootViewProvider, clickAnnotation.value()).setOnClickListener(listener);
            }
        }
    }

    private static int getLayoutResource(Object o) {
        InjectLayout layoutAnnotation = o.getClass().getAnnotation(InjectLayout.class);
        return layoutAnnotation != null ? layoutAnnotation.value() : -1;
    }

    private static View getViewFromProvider(Object rootViewProvider, int viewId) {
        if (Activity.class.isAssignableFrom(rootViewProvider.getClass())) {
            //noinspection ConstantConditions
            return ((Activity) rootViewProvider).findViewById(viewId);
        } else if (View.class.isAssignableFrom(rootViewProvider.getClass())) {
            //noinspection ConstantConditions
            return ((View) rootViewProvider).findViewById(viewId);
        }
        return null;
    }

}
