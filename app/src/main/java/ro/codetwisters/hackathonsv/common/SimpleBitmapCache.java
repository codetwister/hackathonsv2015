package ro.codetwisters.hackathonsv.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

/**
 * This class takes care of saving bitmaps from a url to sdcard and retrieve them
 */
public enum SimpleBitmapCache {
    INSTANCE;
    private static MessageDigest sha1Digest;

    private Map<String, LoadBitmapTask> taskMap = new HashMap<>();

    public void getBitmap(Context context, String url, BitmapLoadedListener listener) {
        // start task to get the bitmap
        // check if we have the task already running
        LoadBitmapTask task = taskMap.get(url);
        if (task == null) {
            new LoadBitmapTask(context, url, listener).execute();
        }
    }

    public void cancelBitmap(String url) {
        LoadBitmapTask task = taskMap.get(url);
        if (task != null) {
            task.cancel(true);
        }
    }

    private static String sha1(String input) {
        try {
            if (sha1Digest == null) {
                sha1Digest = MessageDigest.getInstance("SHA-1");
            }
            sha1Digest.reset();
            sha1Digest.update(input.getBytes("UTF-8"));
            return byteToHex(sha1Digest.digest());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            // this should not happen... but if it does we need to know immediately
            throw new RuntimeException("this system has not SHA-1 algorithm, or no UTF-8 support, WHAAAT!??!");
        }
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    public interface BitmapLoadedListener {
        public void bitmapLoaded(Bitmap bitmap);
        public void bitmapError();
    }

    private class LoadBitmapTask extends AsyncTask<Void, Void, Bitmap> {

        private Context context;
        private String url;
        private BitmapLoadedListener listener;
        private boolean error;

        private LoadBitmapTask(Context context, String url, BitmapLoadedListener listener) {
            this.context = context;
            this.url = url;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            taskMap.put(url, this);
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            // check if we have the file locally
            if (isCancelled()) return null;
            File imageFile = new File(context.getFilesDir(), sha1(url)+".png");
            if (imageFile.exists()) {
                try {
                    if (isCancelled()) return null;
                    return BitmapFactory.decodeStream(new FileInputStream(imageFile));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    error = true;
                }
            } else {
                // download bitmap
                try {
                    URL  imageUrl = new URL(url);
                    if (isCancelled()) return null;
                    HttpURLConnection urlConnection = (HttpURLConnection) imageUrl.openConnection();
                    if (isCancelled()) return null;
                    Bitmap bmp = BitmapFactory.decodeStream(urlConnection.getInputStream());
                    if (isCancelled()) return null;
                    // store bitmap to file
                    if (!imageFile.createNewFile()) {
                        error = true;
                        return null;
                    }
                    FileOutputStream fos = new FileOutputStream(imageFile);
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                    if (isCancelled()) return null;
                    return bmp;
                } catch (IOException e) {
                    error = true;
                    e.printStackTrace();
                    // TODO log this error
                }
            }

            return null;
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            if (bitmap != null) {
                bitmap.recycle();
            }
            taskMap.remove(url);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            taskMap.remove(url);
            if (error) {
                listener.bitmapError();
            } else {
                listener.bitmapLoaded(bitmap);
            }
        }
    }
}
