package ro.codetwisters.hackathonsv.common;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Utility methods for android UIs
 */
public class UiUtils {

    public static float getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static Bitmap getBitmapFromView(View view) {
        return getBitmapFromView(view, Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888));
    }

    public static Bitmap getBitmapFromView(View view, Bitmap bitmap) {
        bitmap.eraseColor(Color.TRANSPARENT); // erasing the bitmap before drawing a new canvas.
        Canvas c = new Canvas(bitmap);
        // problems have risen from the fact that we call layout on views every time. (white layouts, UI thread messed up, etc)
        // thus we try to devise a simple algorithm to skip calling layout on views that we are absolutely sure that they cannot change.
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(c);
        return bitmap;
    }

    public static float getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }
}
